import { Request, Response, NextFunction } from 'express'

export const unknownEndpoint = (_req: Request, res: Response) => {
	res.status(404).send({error: 'Nothing here!'})
}

export const validatePostUser = (req: Request, res: Response, next: NextFunction) => {
	if(!req.body.username || !req.body.full_name || !req.body.email) {
		res.status(400).send({error: 'Invalid input!'})
	} else {
		next()
	}    
}