import express from 'express'
import router from './forumRouter'
import { unknownEndpoint } from './middleware'
import versionRouter from './versionRouter'

const server = express()

server.use(express.json())
server.use('/version', versionRouter)
server.use('/forum', router)
server.use('/', unknownEndpoint)

export default server