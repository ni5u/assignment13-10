import { Router, Request, Response } from 'express'

const versionRouter = Router()

versionRouter.get('/', async (_req: Request, res: Response) => {
	res.status(200).send('version 2')
})

export default versionRouter