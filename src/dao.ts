import { executeQuery } from './db'

interface User {
    user_id?: number
    username: string
    full_name: string
    email: string
}

interface Post {
    post_id?: number
    user_id: number
    title: string
    content: string
}

interface Comment {
    comment_id?: number
    user_id: number
    post_id: number
    content: string
}

export const getUsers = async () => {
	const query = 'SELECT user_id, username FROM users ORDER BY user_id;'
	const result = await executeQuery(query)
	return result.rows
}

export const getUser = async (id: number) => {
	const params = [id]
	const query = 'SELECT * FROM users WHERE user_id = $1;'
	const result = await executeQuery(query, params)
	const user: User = result.rows[0]
	return user
}

export const addUser = async (user_name: string, full_name: string, email: string) => {
	const query = 'INSERT INTO users (username, full_name, email) VALUES ($1, $2, $3) RETURNING *;'
	const params = [user_name, full_name, email]
	const result = await executeQuery(query, params)
	const newUser: User = result.rows[0]
	console.log(result.rows)
	return newUser
}

export const getPosts = async () => {
	const query = 'SELECT post_id, user_id, title FROM posts ORDER BY post_id;'
	const result = await executeQuery(query)
	return result.rows
}

export const getPost = async (id: number) => {
	const params = [id]
	const postQuery = 'SELECT * FROM posts WHERE post_id = $1;'
	const postResult = await executeQuery(postQuery, params)
	const post = postResult.rows[0]
	const commentsQuery = 'SELECT * FROM comments where post_id = $1 ORDER BY created_at;'
	const commentsResult  = await executeQuery(commentsQuery, params)
	post.comments = commentsResult.rows
	return post
}

export const addPost = async (user_id: number, title: string, content: string) => {
	const query = 'INSERT INTO posts (user_id, title, content) VALUES ($1, $2, $3) RETURNING *;'
	const params = [user_id, title, content]
	const result = await executeQuery(query, params)
	const newPost: Post = result.rows[0]
	return newPost
}

export const getCommentsByUser = async (id: number) => {
	const params = [id]
	const query = 'SELECT * FROM comments WHERE user_id = $1;'
	const result = await executeQuery(query, params)
	const comments: Comment[] = result.rows
	return comments
}

export const addComment = async (user_id: number, post_id: number, content: string) => {
	const query = 'INSERT INTO comments (user_id, post_id, content) VALUES ($1, $2, $3) RETURNING *;'
	const params = [user_id, post_id, content]
	const result = await executeQuery(query, params)
	const newPost: Post = result.rows[0]
	return newPost
}

export const deleteUser = async (id: number) => {
	const params = [id]
	const query = 'DELETE FROM users WHERE user_id = $1 RETURNING *;'
	const result = await executeQuery(query, params)
	return result.rows[0]
}

export const deletePost = async (id: number) => {
	const params = [id]
	const query = 'DELETE FROM posts WHERE post_id = $1 RETURNING *;'
	const result = await executeQuery(query, params)
	return result.rows[0]
}
export const deleteComment = async (id: number) => {
	const params = [id]
	const query = 'DELETE FROM comments WHERE comment_id = $1 RETURNING *;'
	const result = await executeQuery(query, params)
	return result.rows[0]
}

// export const updateProduct = async (id: number, product: Product) => {
//     let params
//     let query
//     if(product.name !== undefined && product.price !== undefined) {
//         params = [product.name, product.price, id]
//         query = 'UPDATE products SET name = $1, price = $2 WHERE id = $3 RETURNING id, name, price;'
//     } else if (product.name === undefined) {
//         params = [product.price, id]
//         query = 'UPDATE products SET price = $1 WHERE id = $2 RETURNING id, name, price;'
//     } else {
//         params = [product.name, id]
//         query = 'UPDATE products SET name = $1 WHERE id = $2 RETURNING id, name, price;'
//     }    
//     const result = await executeQuery(query, params)
//     return result
// }



