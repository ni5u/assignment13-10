import server from './server'

const { PORT } = process.env


server.listen(PORT, () => {
	console.log('forum API listening to port', PORT)
})