import { Router, Request, Response } from 'express'
import {  getUsers, getUser, addUser, getPosts, getPost, addPost, getCommentsByUser, addComment, deleteUser, deletePost, deleteComment} from './dao'
import { validatePostUser } from './middleware'

interface User {
    user_id?: number
    username: string
    full_name: string
    email: string
}

interface Post {
    post_id?: number
    user_id: number
    title: string
    content: string
}

interface Comment {
    comment_id?: number
    user_id: number
    post_id: number
    content: string
}
const router = Router()

router.get('/users', async (req: Request, res: Response) => {
	const result = await getUsers()
	res.status(200).send(result)
})

router.get('/users/:id', async (req: Request, res: Response) => {
	const id = Number(req.params.id)
	const user: User = await getUser(id)
	res.status(200).send(user)
})

router.post('/users', validatePostUser, async (req: Request, res: Response) => {
	const user: User = req.body
	const result = await addUser(user.username, user.full_name, user.email)
	res.status(201).send(result)
})

router.delete('/users/:id', async (req: Request, res: Response) => {
	const id = Number(req.params.id)
	const user = await deleteUser(id)
	res.status(200).send(user)
})

router.get('/posts', async (req: Request, res: Response) => {
	const result = await getPosts()
	res.status(200).send(result)
})

router.get('/posts/:id', async (req: Request, res: Response) => {
	const id = Number(req.params.id)
	const result = await getPost(id)
	res.status(200).send(result)
})

router.post('/posts', async (req: Request, res: Response) => {
	const post: Post = req.body
	const result = await addPost(post.user_id, post.title, post.content)
	res.status(201).send(result)
})

router.delete('/post/:id', async (req: Request, res: Response) => {
	const id = Number(req.params.id)
	const post = await deletePost(id)
	res.status(200).send(post)
})

router.get('/comments/:id', async (req: Request, res: Response) => {
	const id = Number(req.params.id)
	const result = await getCommentsByUser(id)
	res.status(200).send(result)
})

router.post('/comment', async (req: Request, res: Response) => {
	const comment: Comment = req.body
	const result = await addComment(comment.user_id, comment.post_id, comment.content)
	res.status(201).send(result)
})

router.delete('/comment/:id', async (req: Request, res: Response) => {
	const id = Number(req.params.id)
	const comment = await deleteComment(id)
	res.status(200).send(comment)
})

export default router