import server from '../src/server'
import request from 'supertest'
import { jest } from '@jest/globals'
import { pool } from '../src/db'

const initializeMockPool = (mockResponse: any) => {
	(pool as any).connect = jest.fn(() => {
		return {
			query: () => mockResponse,
			release: () => null
		}
	})
}


describe('Testing GET /forum/users', () => {
	const mockResponse = {
		rows: [
			{
				'user_id': 1,
				'username': 'nisu'
			},
			{
				'user_id': 3,
				'username': 'hessu'
			}
		]
	}

	beforeAll(() => {
		initializeMockPool(mockResponse)
	})

	afterAll(() => {
		jest.clearAllMocks()
	})

	it('GET /users should return 200 and a list of users', async () => {
		const response = await request(server).get('/forum/users')
		expect(response.statusCode).toBe(200)
		expect(response.body).toStrictEqual(mockResponse.rows)
	})
})


describe('Testing GET /forum/users/:id', () => {

	const mockResponse = {
		rows: [{
			'user_id': 1,
			'username': 'nisu',
			'full_name': 'Sami Nissinen',
			'email': 'nisu@nisula.com'
		}]
	}

	beforeAll(() => {
		initializeMockPool(mockResponse)
	})

	afterAll(() => {
		jest.clearAllMocks()
	})

	it('GET /users/:id should return 200 and user_id, username, fullname and email', async () => {
		const response = await request(server).get('/forum/users/:id')
		expect(response.statusCode).toBe(200)
		expect(response.body).toStrictEqual(mockResponse.rows[0])
	})

      
})

describe('Testing POST /forum/users/', () => {

	const mockResponse = {
		rows: [{
			'user_id': 12,
			'username': 'Hellevi',
			'full_name': 'Hellä Ellevi',
			'email': 'tirri@tomppa.com'
		}]
	}

	beforeAll(() => {
		initializeMockPool(mockResponse)
	})

	afterAll(() => {
		jest.clearAllMocks()
	})

	it('POST /forum/users/ should return 200 with correct input ', async () => {
		const response = await request(server).post('/forum/users/').send({
			'username': 'Hellevi',
			'full_name': 'Hellä Ellevi',
			'email': 'tirri@tomppa.com'
		})
		expect(response.statusCode).toBe(201)
		expect(response.body).toStrictEqual(mockResponse.rows[0])
	})

	it('POST /forum/users/ should return 400 with incorrect input ', async () => {
		const response = await request(server).post('/forum/users/').send({
			'usern': 'Hellevi',         //typo in username
			'full_name': 'Hellä Ellevi',
			'email': 'tirri@tomppa.com'
		})
		expect(response.statusCode).toBe(400)
	}) 
      
})

describe('Testing DELETE /forum/user/:id', () => {

	const mockResponse = {
		rows: [{
			'user_id': 12,
			'username': 'Hellevi',
			'full_name': 'Hellä Ellevi',
			'email': 'tirri@tomppa.com'
		}]
	}

	beforeAll(() => {
		initializeMockPool(mockResponse)
	})

	afterAll(() => {
		jest.clearAllMocks()
	})

	it('DELETE /forum/users/:id should return 200 with correct input and info about deleted user', async () => {
		const response = await request(server).delete('/forum/users/:12')
		expect(response.statusCode).toBe(200)
		expect(response.body).toStrictEqual(mockResponse.rows[0])
	})

      
})

describe('Testing GET /posts', () => {

	const mockResponse = {
		rows: [{
			'post_id': 1,
			'user_id': 1,
			'title': 'Testing'
		},
		{
			'post_id': 5,
			'user_id': 3,
			'title': 'Terve Kalle!'
		}]
	}

	beforeAll(() => {
		initializeMockPool(mockResponse)
	})

	afterAll(() => {
		jest.clearAllMocks()
	})

	it('GET /forum/posts should return 200 with a list of posts that have correct properties', async () => {
		const response = await request(server).get('/forum/posts')
		expect(response.statusCode).toBe(200)
		console.log(response.body.rows)
		expect(response.body).toHaveProperty([0,'post_id'])
		expect(response.body).toHaveProperty([0,'user_id'])
		expect(response.body).toHaveProperty([0,'title'])
	})
      
})

describe('Testing GET /forum/posts/:id', () => {

	const mockResponse = {
		rows: [{
			'post_id': 1,
			'user_id': 1,
			'title': 'Testing',
			'content': 'This is a test',
			'created_at': '2023-06-15T02:56:23.820Z',
			'comments': [
				{
					'comment_id': 1,
					'user_id': 1,
					'post_id': 1,
					'content': 'Testataanpa kommenttejakin',
					'created_at': '2023-06-15T03:07:00.722Z'
				},
				{
					'comment_id': 6,
					'user_id': 5,
					'post_id': 1,
					'content': 'Test',
					'created_at': '2023-06-15T03:14:03.095Z'
				}
			]
		}]
	}

	beforeAll(() => {
		initializeMockPool(mockResponse)
	})

	afterAll(() => {
		jest.clearAllMocks()
	})

	it('GET /users/:id should return 200 and user_id, username, fullname and email', async () => {
		const response = await request(server).get('/forum/users/:id')
		expect(response.statusCode).toBe(200)
		expect(response.body).toStrictEqual(mockResponse.rows[0])
	})
      
})

// to be continued
